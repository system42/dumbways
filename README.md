1.	DevOps ada gabungan dari developpment sampai dengan Operasional suatu aplikasi.

Penting disuatu perusahaan karena dengan penerapan DevOps maka proses deployment aplikasi mulai dari plan sampai production dapat berjalan dengan baik dan process monitoring juga dapat terkontrol dengan baik.

Untuk Flow dimulai dari Development yaitu Plan -> Code -> Build -> Test kemudian Release, selanjutnya Operation akan melakukan Deploy  Operate dan Monitoring, jika ada bug atau update di sisi aplikasi maka flow akan di ulang Kembali dari Plan. Sehingga Logo dari pada DevOps seperti berikut :

2.	Untuk infra AWS, sebenarnya kita harus melihat dari sisi mana yang akan kita gunakan dan juga budgeting yang kita miliki, jika kita hanya ingin memanage vm dan isi dari vm kita yang manage sendiri, maka yang dibutuhkan adalah :
a.	Region mana akan di piliah
b.	VPC untuk mengatur letak server yang akan di akses  public dan local
c.	EC2 untuk tempat kita mendeploy server-server kita, jika kita mau install sendiri Kubernetes untuk implementasi container maka kita tinggal pakai EKS.
d.	CloudWatch bisa kita gunakan sebagai monitoring
e.	RDS sebagai database relational, jika no SQL bisa menggunakan MongoDB.
f.	ELB untuk Load Balancer
g.	IAM untuk security access control
h.	Dan lain-lain sebab banyak feature dari aws tergantung design kita, untuk hal diatas saja sudah bisa digunakan.

3.	GIT adalah suatu tools yang digunakan oleh para developer biasa untuk melakukan management code yang dibuat oleh mereka, git akan bekerja dengan model versioning dari setiap upload yang dilakukan, jadi GIT client akan di install di sisi PC developer dan setiap developer selesai membuat code dan ingin di upload ke server maka developer akan melakukan git pull ke arah git server dan kemudian di sisi server akan melakukan git pull untuk mengambil codenya dari git server (hal ini bisa dilakukan manual atau pun automatic)

4.	CI/CD, terdiri dari 2 process yaitu CI dan CD, CI adalah Continuous Integration disini para developer akan melakukan integrasi kearah repository jika commit dan test sudah di lakukan oleh developer, Sedangkan CD terdiri dari Continuous Delivery and Continous Development, dimana setelah CI sukses, maka akan di release ke production server dalam hal ini di disebut dengan Deployment dan bisa dilakukan secara automatic ataupun manual tergantung dari perusahaan, disinilah maksud dari CD tersebut.

Untuk Flow, CI -> Code -> Build -> test -> push to repository
Next ke CD -> release -> Deploy > Operate 

5.	Video AddUser 

6.	Video GitPush

7.	NMS banyak yang bisa digunakan seperti Nagios, Datadog, Prometheus dan lain-lain sekarang yang harus di tentukan adalah protocol dan model yang akan digunakan untuk memonitoring server kita bisa via SNMP protocol atau agent based. Untuk SNMP Protocol biasanya Nagios, Cacti dan beberapa tools lainya sedangkan agent based bisa datadog, Prometheus site24x7.

Untuk Flow SNMP , Monitoring Server akan memonitoring server dengan mengakses SNMP Server yang di install di sisi Server.
Untuk Flow agent based, Monitoring Server akan menerima raw data dari server yang dimonitor via agent yang telah di install disisi server yang akan dimonitoring.

8.	Video CreateWebServer
